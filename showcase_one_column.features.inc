<?php
/**
 * @file
 * showcase_one_column.features.inc
 */

/**
 * Implements hook_node_info().
 */
function showcase_one_column_node_info() {
  $items = array(
    'showcase_onecol' => array(
      'name' => t('Showcase (one column)'),
      'base' => 'node_content',
      'description' => t('Used to create a one-column showcase widget, for display on pages and webforms.'),
      'has_title' => '1',
      'title_label' => t('Header'),
      'help' => t('<strong>One column showcase Image size info</strong>
<br />
429px by 221px'),
    ),
  );
  return $items;
}
