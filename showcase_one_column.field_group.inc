<?php
/**
 * @file
 * showcase_one_column.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function showcase_one_column_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sc_onecol_item|field_collection_item|field_sc_onecol_item|form';
  $field_group->group_name = 'group_sc_onecol_item';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_sc_onecol_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Item',
    'weight' => '0',
    'children' => array(
      0 => 'field_sc_onecol_item_caption',
      1 => 'field_sc_onecol_item_image',
      2 => 'field_sc_onecol_item_type',
      3 => 'field_sc_onecol_item_video',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Item',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_sc_onecol_item|field_collection_item|field_sc_onecol_item|form'] = $field_group;

  return $export;
}
